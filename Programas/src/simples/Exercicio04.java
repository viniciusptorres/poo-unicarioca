package simples;

import java.util.Scanner;

public class Exercicio04 {
	
	//Fun��o que processa o que ser� exibido ao usu�rio
	public static double calcular(double comprimento, double largura, double potencia) {	
		return (comprimento*largura*18)/potencia;
	}
	
	//Bloco principal do programa
	public static void main(String[] args) {

		//Declara��o das vari�veis
		int potencia;
		double lampada, largura, comprimento;
												
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner(System.in);
		System.out.println("Informe a potencia das lampadas:");
		potencia = entrada.nextInt();
		System.out.println("Informe o comprimento do comodo:");
		comprimento = entrada.nextDouble();
		System.out.println("Informe a largura do comodo:");
		largura = entrada.nextDouble();
		entrada.close();
												
		/* Chamando a fun��o que executa o processamento de dados do programa e 
		 * atribuindo seu resultado a uma vari�vel para exibi��o posterior ao usu�rio */
		lampada = calcular(comprimento, largura, potencia);				
												
		//Sa�da do processamento para o usu�rio
		System.out.println("O n�mero de l�mpadas necessarias �: "+lampada);		

	}

}
