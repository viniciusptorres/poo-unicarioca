package simples;

import java.util.Scanner;

public class Exercicio01 {

	//Fun��o que processa o que ser� exibido ao usu�rio
	public static double calcular(double raio) {	
		return Math.pow(raio, 2) * 3.1415926535898;
	}
	
	//Bloco principal do programa
	public static void main(String[] args) {

		//Declara��o das vari�veis
		double raio;
						
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner (System.in);
		System.out.println("Informe o raio do c�rculo (em cm): ");
		raio = entrada.nextDouble();
		entrada.close();
										
		/* Chamando a fun��o que executa o processamento de dados do programa e 
		 * atribuindo seu resultado a uma vari�vel para exibi��o posterior ao usu�rio */
		raio = calcular(raio);
										
		//Exibi��o do resultado do processamento para o usu�rio
		System.out.println("A �rea do c�rculo �: "+raio+" cm�");

	}

}
