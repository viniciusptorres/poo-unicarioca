package simples;

import java.util.Scanner;

public class Exercicio02 {

	//Fun��o que processa o que ser� exibido ao usu�rio
	public static double converter(double fahrenheit) {	
		return (fahrenheit-32)/9*5;
	}
	
	//Bloco principal de programa
	public static void main(String[] args) {

		//Declara��o das vari�veis
		double fahrenheit;
								
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner (System.in);
		System.out.println("Informe a temperatura em Fahrenheit: ");
		fahrenheit = entrada.nextDouble();
		entrada.close();
				
		/* Chamando a fun��o que executa o processamento de dados do programa e 
		 * atribuindo seu resultado a uma vari�vel para exibi��o posterior ao usu�rio */
		fahrenheit = converter(fahrenheit);
								
		//Sa�da do processamento para o usu�rio
		System.out.println("A temperatura em Celsius �: "+fahrenheit+" graus");

	}

}
