package simples;

import java.util.Scanner;

public class Exercicio07 {

	//Fun��o que processa o que ser� exibido ao usu�rio
	public static double calcular(double corrida, double comprimento, double consumo, double parada) {	
		return ((corrida*(comprimento/1000))/consumo)/(parada+1);
	}
		
	//Bloco principal do programa
	public static void main(String[] args) {
		
		//Declara��o das vari�veis
		double comprimento, corrida, parada, consumo, combustivel;
																				
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner(System.in);
		System.out.println("Informe o comprimento da pista em metros:");
		comprimento = entrada.nextDouble();
		System.out.println("Informe o total de voltas da corrida:");
		corrida = entrada.nextDouble();
		System.out.println("Informe o total de paradas para reabastecimento:");
		parada = entrada.nextDouble();
		System.out.println("Informe o consumo de combust�vel do carro em Km/L:");
		consumo = entrada.nextDouble();
		entrada.close();
																				
		/* Chamando as fun��es que executam o processamento de dados do programa e 
		 * atribuindo seus resultados a vari�veis para exibi��o posterior ao usu�rio */
		combustivel = calcular(corrida, comprimento, consumo, parada);
																		
		//Sa�da do processamento para o usu�rio
		System.out.println("O total de combust�vel necess�rio at� o primeiro reabastecimento � de: "+combustivel+" Litros.");

	}

}
