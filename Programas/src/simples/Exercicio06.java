package simples;

import java.util.Scanner;

public class Exercicio06 {

	//Fun��o que processa o que ser� exibido ao usu�rio (Consumo de combust�vel)
	public static double consumir(double km1, double km2, double combustivel) {	
		return (km2-km1)/combustivel;
	}
	
	//Fun��o que processa o que ser� exibido ao usu�rio (Lucro l�quido)
	public static double lucrar(double dinheiro, double combustivel) {	
		return dinheiro-(combustivel*1.9);
	}
		
	//Bloco principal do programa
	public static void main(String[] args) {

		//Declara��o das vari�veis
		double km1, km2, combustivel, dinheiro, consumo, lucro;		
																		
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner(System.in);
		System.out.println("Informe a quilometragem no in�cio do dia:");
		km1 = entrada.nextDouble();
		System.out.println("Informe a quilometragem no final do dia:");
		km2 = entrada.nextDouble();
		System.out.println("Informe quantos litros de combustivel foram gastos no dia:");
		combustivel = entrada.nextDouble();
		System.out.println("Informe o total de dinheiro obtido no dia:");
		dinheiro = entrada.nextDouble();
		entrada.close();
																		
		/* Chamando as fun��es que executam o processamento de dados do programa e 
		 * atribuindo seus resultados a vari�veis para exibi��o posterior ao usu�rio */
		consumo = consumir(km1, km2, combustivel);
		lucro = lucrar(dinheiro, combustivel);
																
		//Sa�da do processamento para o usu�rio
		System.out.println("O consumo m�dio de combustivel foi de: "+consumo+" Km/L e o Lucro L�quido foi de: R$"+lucro);

	}

}
