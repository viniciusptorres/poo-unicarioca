package simples;

import java.util.Scanner;

public class Exercicio05 {

	//Fun��o que processa o que ser� exibido ao usu�rio
	public static double calcular(double altura, double comprimento, double largura) {	
		return ((comprimento*altura*2)+(largura*altura*2))/1.5;
	}
	
	//Bloco principal do programa
	public static void main(String[] args) {

		//Declara��o das vari�veis
		double caixa, comprimento, largura, altura;
																
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner(System.in);
		System.out.println("Informe o comprimento da cozinha:");
		comprimento = entrada.nextDouble();
		System.out.println("Informe a largura da cozinha:");
		largura = entrada.nextDouble();
		System.out.println("Informe a altura da cozinha:");
		altura = entrada.nextDouble();
		entrada.close();
																
		/* Chamando a fun��o que executa o processamento de dados do programa e 
		 * atribuindo seu resultado a uma v�riavel para exibi��o posterior ao usu�rio */
		caixa = calcular(altura, comprimento, largura);
														
		//Sa�da do processamento para o usu�rio
		System.out.println("O n�mero de caixas de azulejos necess�rias �: "+caixa);

	}

}
