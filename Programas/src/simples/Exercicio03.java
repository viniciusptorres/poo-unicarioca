package simples;

import java.util.Scanner;

public class Exercicio03 {

	//Fun��o que processa o que ser� exibido ao usu�rio
	public static double converter(double celsius) {	
		return (celsius*9/5)+32;
	}
	
	//Bloco principal do programa
	public static void main(String[] args) {

		//Declara��o das vari�veis
		double celsius;
										
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner (System.in);
		System.out.println("Informe a temperatura em Celsius: ");
		celsius = entrada.nextDouble();
		entrada.close();
										
		/* Chamando a fun��o que executa o processamento de dados do programa e 
		 * atribuindo seu resultado a uma vari�vel para exibi��o posterior ao usu�rio */
		celsius = converter(celsius);
										
		//Sa�da do processamento para o usu�rio
		System.out.println("A temperatura em Fahrenheit �: "+celsius+" graus");
	}

}
