package vetores;

import java.util.Scanner;

public class Exercicio08 {

	//Bloco principal do programa
	public static void main(String[] args) {
						
		//Declara��o das vari�veis
		double W[] = new double[10];
		double V; 
		int i;
		int cont = 0;
		String indice = "";
			
		//Leitura dos dados a serem inseridos
		Scanner entrada = new Scanner (System.in);
		for(i=0;i<=9;i++) {
			System.out.println("Insira um valor:");
			W[i] = entrada.nextDouble();			
		}
		System.out.println("Insira o valor que ser� procurado:");
		V = entrada.nextDouble();
		entrada.close();
			
		//Processamento de dados do programa
		for(i=0;i<=9;i++) {
			if(W[i]==V) {
				cont++;
				indice += i+"\t";
			}							
		}						
			
		//Exibi��o do resultado do processamento para o usu�rio
		for(i=0;i<=9;i++) 
			System.out.printf("%s\t",W[i]);
		System.out.println("");
		if(cont==0) 
			System.out.println("O n�mero "+V+" n�o foi inserido.");
		else {
			System.out.println("Total de entradas do n�mero "+V+": "+cont);
			System.out.println("�ndices nos quais o n�mero "+V+" Aparece: "+indice);
		}
		
	}

}
