package vetores;

public class Exercicio01 {

	//Bloco principal do programa
	public static void main(String[] args) {
		
		//Declara��o das vari�veis
		int X[] = new int[10];
		int i;
		
		//Processamento de dados do programa
		for(i=0;i<=9;i++) 
			X[i]=30;			
		
		//Exibi��o do resultado do processamento para o usu�rio
		for(i=0;i<=9;i++) 
			System.out.printf("%d\t",X[i]);
		
	}	

}