package vetores;

import java.util.Scanner;

public class Exercicio07 {

	//Bloco principal do programa
	public static void main(String[] args) {
							
		//Declara��o das vari�veis
		int X[] = new int[10];
		int Y[] = new int[10];
		int i;
				
		//Leitura dos dados a serem inseridos
		Scanner entrada = new Scanner (System.in);
		for(i=0;i<=9;i++) {
			System.out.println("Insira um valor inteiro:");
			X[i] = entrada.nextInt();			
		}
		entrada.close();
				
		//Processamento de dados do programa
		for(i=0;i<=9;i++) {
			if(i%2==0)
				Y[i]=X[i]/2;
			else
				Y[i]=X[i]*3;
		}						
				
		//Exibi��o do resultado do processamento para o usu�rio
		System.out.println("Vetor X:");
		for(i=0;i<=9;i++) 
			System.out.printf("%d\t",X[i]);
		System.out.println("");
		System.out.println("Vetor Y:");
		for(i=0;i<=9;i++) 
			System.out.printf("%d\t",Y[i]);

	}

}
