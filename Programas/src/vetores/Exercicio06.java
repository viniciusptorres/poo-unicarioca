package vetores;

import java.util.Scanner;

public class Exercicio06 {

	//Bloco principal do programa
	public static void main(String[] args) {
						
		//Declara��o das vari�veis
		double D[] = new double[10];
		double E[] = new double[10];
		int i;
			
		//Leitura dos dados a serem inseridos
		Scanner entrada = new Scanner (System.in);
		for(i=0;i<=9;i++) {
			System.out.println("Insira um valor:");
			D[i] = entrada.nextDouble();			
		}
		entrada.close();
			
		//Processamento de dados do programa
		for(i=0;i<=9;i++) {
			E[i]=D[9-i];
		}						
			
		//Exibi��o do resultado do processamento para o usu�rio
		System.out.println("Vetor D:");
		for(i=0;i<=9;i++) 
			System.out.printf("%s\t",D[i]);
		System.out.println("");
		System.out.println("Vetor E:");
		for(i=0;i<=9;i++) 
			System.out.printf("%s\t",E[i]);

	}

}