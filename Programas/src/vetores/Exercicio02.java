package vetores;

public class Exercicio02 {

	//Bloco principal do programa
	public static void main(String[] args) {
			
		//Declara��o das vari�veis
		int A[] = new int[10];
		int i;
		
		//Processamento de dados do programa
		for(i=0;i<=9;i++) 
			A[i]=i+1;			
		
		//Exibi��o do resultado do processamento para o usu�rio
		for(i=0;i<=9;i++) 
			System.out.printf("%d\t",A[i]);
			
	}

}