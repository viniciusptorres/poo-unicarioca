package vetores;

import java.util.Scanner;

public class Exercicio05 {

	//Bloco principal do programa
	public static void main(String[] args) {
					
		//Declara��o das vari�veis
		int C[] = new int[10];
		int i;
		
		//Leitura dos dados a serem inseridos
		Scanner entrada = new Scanner (System.in);
		for(i=0;i<=9;i++) {
			System.out.println("Insira um valor inteiro:");
			C[i] = entrada.nextInt();			
		}
		entrada.close();
		
		//Processamento de dados do programa
		for(i=0;i<=9;i++) {
			if(C[i]<0)
				C[i]=0;
		}						
		
		//Exibi��o do resultado do processamento para o usu�rio
		for(i=0;i<=9;i++) 
			System.out.printf("%d\t",C[i]);

	}

}