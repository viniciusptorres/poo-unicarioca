package vetores;

public class Exercicio03 {

	//Bloco principal do programa
	public static void main(String[] args) {
				
		//Declara��o das vari�veis
		int B[] = new int[10];
		int i;
		
		//Processamento de dados do programa
		for(i=0;i<=9;i++) {
			if(i%2==0)
				B[i]=0;
			else
				B[i]=1;
		}						
		
		//Exibi��o do resultado do processamento para o usu�rio
		for(i=0;i<=9;i++) 
			System.out.printf("%d\t",B[i]);

	}

}