package matrizes;

import java.util.Scanner;

public class Exercicio08 {

	//Bloco principal do programa
	public static void main(String[] args) {
									
		//Declara��o das vari�veis
		String[] turno = {"Manh�","Tarde"};
		String[] dia = {"Segunda","Ter�a","Quarta","Quinta","Sexta"};
		String rendimento;
		double M[][] = new double[2][5];
		double maior;
		int i, j;		
						
		//Leitura dos dados a serem inseridos
		Scanner entrada = new Scanner (System.in);
		for(j=0;j<=4;j++) {
			for(i=0;i<=1;i++) {
				System.out.println("Insira o valor da venda ("+dia[j]+"-"+turno[i]+"):");
				M[i][j] = entrada.nextDouble();
			}						
		}
		entrada.close();
				
		//Processamento de dados do programa
		maior = M[0][0];
		rendimento = "\t"+dia[0]+"-"+turno[0];
		for(j=0;j<=0;j++) {
			for(i=1;i<=1;i++) {
				if(M[i][j]==maior)
					rendimento += "\t"+dia[j]+"-"+turno[i];				
				else if(M[i][j]>maior) {
					maior = M[i][j];
					rendimento = "\t"+dia[j]+"-"+turno[i];
				}					
			}
		}					
		for(j=1;j<=4;j++) {
			for(i=0;i<=1;i++) {
				if(M[i][j]==maior)
					rendimento += "\t"+dia[j]+"-"+turno[i];				
				else if(M[i][j]>maior) {
					maior = M[i][j];
					rendimento = "\t"+dia[j]+"-"+turno[i];
				}					
			}
		}		
							
		//Exibi��o do resultado do processamento para o usu�rio
		for(i=0;i<=1;i++) {
			for(j=0;j<=4;j++)
				System.out.printf("%s\t",M[i][j]);
			System.out.println("");
		}
		System.out.println("");
		System.out.println("Dia(s) e turno(s) com maior rendimento: "+rendimento);

	}
	
}