package matrizes;

import java.util.Scanner;

public class Exercicio04 {

	//Bloco principal do programa
	public static void main(String[] args) {
							
		//Declara��o das vari�veis
		double SOMA[][] = new double[4][4];
		int i, j, somalinha, somacoluna, somaelemento;
		somalinha = somacoluna = somaelemento = 0;
				
		//Leitura dos dados a serem inseridos
		Scanner entrada = new Scanner (System.in);
		for(i=0;i<=3;i++) {
			for(j=0;j<=3;j++) {
				System.out.println("Insira um valor:");
				SOMA[i][j] = entrada.nextInt();
			}						
		}
		entrada.close();
		
		//Processamento de dados do programa
		for(i=2;i<=2;i++)		//Soma dos elementos da linha 3
			for(j=0;j<=3;j++)
				somalinha += SOMA[i][j];
					
		for(i=0;i<=3;i++)		//Soma dos elementos da coluna 2
			for(j=1;j<=1;j++)
				somacoluna += SOMA[i][j];
					
		for(i=0;i<=3;i++)		//Soma de todos os elementos
			for(j=0;j<=3;j++)
				somaelemento += SOMA[i][j];
					
		//Exibi��o do resultado do processamento para o usu�rio
		for(i=0;i<=3;i++) {
			for(j=0;j<=3;j++)
				System.out.printf("%s\t",SOMA[i][j]);
			System.out.println("");
		}
		System.out.println("");
		System.out.println("Soma dos elementos da linha 3: "+somalinha);
		System.out.println("Soma dos elementos da coluna 2: "+somacoluna);
		System.out.println("Soma de todos os elementos da matriz: "+somaelemento);
	}

}