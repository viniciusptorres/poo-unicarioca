package matrizes;

import java.util.Scanner;

public class Exercicio07 {

	//Bloco principal do programa
	public static void main(String[] args) {
							
		//Declara��o das vari�veis
		int M[][] = new int[4][4];
		int i, j, X;
					
		//Leitura dos dados a serem inseridos
		Scanner entrada = new Scanner (System.in);
		for(i=0;i<=3;i++) {
			for(j=0;j<=3;j++) {
				System.out.println("Insira um valor inteiro:");
				M[i][j] = entrada.nextInt();
			}						
		}
		System.out.println("Insira um valor inteiro a ser multiplicado:");
		X = entrada.nextInt();
		entrada.close();
			
		//Processamento de dados do programa
		for(i=0;i<=3;i++) {
			for(j=0;j<=3;j++) {
				if(i==j)
					M[i][j] *= X;								
			}				
		}
				
		//Exibi��o do resultado do processamento para o usu�rio
		System.out.println("Matriz resultante:");
		for(i=0;i<=3;i++) {
			for(j=0;j<=3;j++)
				System.out.printf("%d\t",M[i][j]);
			System.out.println("");
		}
		
	}
		
}