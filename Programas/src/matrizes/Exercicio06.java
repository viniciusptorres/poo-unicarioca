package matrizes;

import java.util.Scanner;

public class Exercicio06 {

	//Bloco principal do programa
	public static void main(String[] args) {
									
		//Declara��o das vari�veis
		double A[][] = new double[4][6];
		double B[][] = new double[4][6];
		double S[][] = new double[4][6];
		double D[][] = new double[4][6];
		int i, j;		
						
		//Leitura dos dados a serem inseridos
		Scanner entrada = new Scanner (System.in);
		for(i=0;i<=3;i++) {		//Leitura da matriz A
			for(j=0;j<=5;j++) {
				System.out.println("Insira um valor (Matriz A):");
				A[i][j] = entrada.nextInt();
			}						
		}
		
		for(i=0;i<=3;i++) {		//Leitura da Matriz B
			for(j=0;j<=5;j++) {
				System.out.println("Insira um valor (Matriz B):");
				B[i][j] = entrada.nextInt();
			}						
		}
		entrada.close();
				
		//Processamento de dados do programa				
		for(i=0;i<=3;i++)		//Cria��o da matriz S
			for(j=0;j<=5;j++)
				S[i][j] = A[i][j]+B[i][j];
							
		for(i=0;i<=3;i++)		//Cria��o da matriz D
			for(j=0;j<=5;j++)
				D[i][j] = A[i][j]-B[i][j];
							
		//Exibi��o do resultado do processamento para o usu�rio
		System.out.println("Matriz A");
		for(i=0;i<=3;i++) {		//Exibi��o da matriz A
			for(j=0;j<=5;j++)
				System.out.printf("%s\t",A[i][j]);
			System.out.println("");
		}
		System.out.println("");
		
		System.out.println("Matriz B");
		for(i=0;i<=3;i++) {		//Exibi��o da matriz B
			for(j=0;j<=5;j++)
				System.out.printf("%s\t",B[i][j]);
			System.out.println("");
		}
		System.out.println("");
		
		System.out.println("Matriz S");
		for(i=0;i<=3;i++) {		//Exibi��o da matriz S
			for(j=0;j<=5;j++)
				System.out.printf("%s\t",S[i][j]);
			System.out.println("");
		}
		System.out.println("");
		
		System.out.println("Matriz D");
		for(i=0;i<=3;i++) {		//Exibi��o da matriz D
			for(j=0;j<=5;j++)
				System.out.printf("%s\t",D[i][j]);
			System.out.println("");
			
		}
		
	}
	
}