package matrizes;

import java.util.Scanner;

public class Exercicio03 {

	//Bloco principal do programa
	public static void main(String[] args) {
						
		//Declara��o das vari�veis
		int D[][] = new int[3][3];
		int i, j, X;
		boolean condicao = false;
			
		//Leitura dos dados a serem inseridos
		Scanner entrada = new Scanner (System.in);
		for(i=0;i<=2;i++) {
			for(j=0;j<=2;j++) {
				System.out.println("Insira um valor inteiro:");
				D[i][j] = entrada.nextInt();
			}						
		}
		System.out.println("Insira um valor inteiro a ser verificado:");
		X = entrada.nextInt();
		entrada.close();
		
		//Processamento de dados do programa
		for(i=0;i<=2;i++) {
			for(j=0;j<=2;j++) {
				if(D[i][j]==X) {
					condicao = true;
					break; //Interrompe o looping, j� que nenhum n�mero se repete na matriz
				}					
			}				
		}
			
		//Exibi��o do resultado do processamento para o usu�rio
		for(i=0;i<=2;i++) {
			for(j=0;j<=2;j++)
				System.out.printf("%d\t",D[i][j]);
			System.out.println("");
		}
		System.out.println("");
		if(condicao==true)
			System.out.println("O valor "+X+" EXISTE na matriz.");
		else
			System.out.println("O valor "+X+" N�O EXISTE na matriz.");
	}

}