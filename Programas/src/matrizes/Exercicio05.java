package matrizes;

import java.util.Scanner;

public class Exercicio05 {

	//Bloco principal do programa
	public static void main(String[] args) {
								
		//Declara��o das vari�veis
		double G[][] = new double[3][3];
		double[] SL = {0,0,0};
		double[] SC = {0,0,0};
		int i, j;		
					
		//Leitura dos dados a serem inseridos
		Scanner entrada = new Scanner (System.in);
		for(i=0;i<=2;i++) {
			for(j=0;j<=2;j++) {
				System.out.println("Insira um valor:");
				G[i][j] = entrada.nextInt();
			}						
		}
		entrada.close();
			
		//Processamento de dados do programa				
		for(i=0;i<=2;i++)		//Soma dos elementos das linhas, armazenadas no vetor SL
			for(j=0;j<=2;j++)
				SL[i] += G[i][j];
						
		for(j=0;j<=2;j++)		//Soma dos elementos das colunas, armazenadas no vetor SC
			for(i=0;i<=2;i++)
				SC[j] += G[i][j];
						
		//Exibi��o do resultado do processamento para o usu�rio
		for(i=0;i<=2;i++) {
			for(j=0;j<=2;j++)
				System.out.printf("%s\t",G[i][j]);
			System.out.println("");
		}
		System.out.println("");
		System.out.printf("Vetor SL: ");
		for(i=0;i<=2;i++)
			System.out.printf("%s\t",SL[i]);
		System.out.println("");
		System.out.printf("Vetor SC: ");	
		for(j=0;j<=2;j++)
			System.out.printf("%s\t",SC[j]);
	}

}
