package repeticao;

import java.util.Scanner;

public class Exercicio03 {

	//Fun��o que processa o que ser� exibido ao usu�rio
	public static double calcular(double num1, double num2) {	
		return num1/num2;
	}
			
	//Bloco principal do programa
	public static void main(String[] args) {
				
		//Declara��o das vari�veis
		double num1, num2, resultado;
		num2 = 0;
				
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner(System.in);
		System.out.println("Informe o primeiro n�mero:");
		num1 = entrada.nextDouble();
		while(num2 == 0) {
			System.out.println("Informe o segundo n�mero:");
			num2 = entrada.nextDouble();			
		}
		entrada.close();
				
		/* Chamando a fun��o que executa o processamento de dados do programa e 
		 * atribuindo seu resultado a uma vari�vel para exibi��o posterior ao usu�rio */
		resultado = calcular(num1, num2);
				
		//Sa�da do processamento para o usu�rio
		System.out.println("O resultado da divis�o �: "+resultado);

	}

}
