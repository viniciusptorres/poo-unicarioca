package repeticao;

import java.util.Scanner;

public class Exercicio05 {

	//Fun��o que processa o que ser� exibido ao usu�rio (M�dia)
	public static double calcular(double av1, double av2) {	
		return (av1+av2)/2;			
	}
			
	//Bloco principal do programa
	public static void main(String[] args) {
				
		//Declara��o das vari�veis
		double av1, av2, media;			
				
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner(System.in);
		do {
			System.out.println("Digite a nota da AV1:");
			av1 = entrada.nextDouble();
			if(av1<0 || av1>10)
				System.out.println("Nota Inv�lida!");
		}
		while(av1<0 || av1>10);
		do {
			System.out.println("Digite a nota da AV2:");
			av2 = entrada.nextDouble();
			if(av2<0 || av2>10)
				System.out.println("Nota Inv�lida!");
		}
		while(av2<0 || av2>10);
		entrada.close();
							
		/* Chamando as fun��es que executam o processamento de dados do programa e 
		 * atribuindo seus resultados a vari�veis para exibi��o posterior ao usu�rio */
		media = calcular(av1, av2);
		
		//Sa�da do processamento para o usu�rio
		System.out.println("A m�dia do aluno �: "+media);
		
	}

}
