package selecao;

import java.util.Scanner;

public class Exercicio02 {

	//Fun��o que processa o que ser� exibido ao usu�rio (M�dia)
	public static double calcular(double av1, double av2, double av3) {	
		if(av1>av3 && av2>av3)
			return (av1+av2)/2;
		else if(av1>av2 && av3>av2)
			return (av1+av3)/2;
		else
			return (av2+av3)/2;
	}
	
	//Fun��o que processa o que ser� exibido ao usu�rio (Status)
	public static String verificar(double media) {	
		if(media>=6)
			return new String("Aprovado!");
		else if(media<6 && media>=3)
			return new String("Exame!");
		else
			return new String("Reprovado!");
	}
		
	//Bloco principal do programa
	public static void main(String[] args) {
		
		//Declara��o das vari�veis
		String status;
		double av1, av2, av3, media;
		int selecao;
		
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner(System.in);
		System.out.println("Digite a nota da AV1:");
		av1 = entrada.nextDouble();
		System.out.println("Digite a nota da AV2:");
		av2 = entrada.nextDouble();
		System.out.println("O Aluno fez a AV3? Digite '0' para sim ou qualquer outro algarismo para n�o:");
		selecao = entrada.nextInt();
		if(selecao == 0) {
			System.out.println("Digite a nota da AV3:");
			av3 = entrada.nextDouble();
		}
		else
			av3 = -1;
		entrada.close();
		
		/* Chamando as fun��es que executam o processamento de dados do programa e 
		 * atribuindo seus resultados a vari�veis para exibi��o posterior ao usu�rio */
		media = calcular(av1, av2, av3);
		status = verificar(media);
		
		//Sa�da do processamento para o usu�rio
		System.out.println("A m�dia do aluno �: "+media);
		System.out.println(status);
		
	}

}
