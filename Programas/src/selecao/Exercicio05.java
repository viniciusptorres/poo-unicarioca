package selecao;

import java.util.Scanner;

public class Exercicio05 {
	
	public static String verificar(double numero) {
		if(numero>=0)
			return new String("O n�mero lido foi: "+numero+". Ele � Positivo.");
		else
			return new String("O n�mero lido foi: "+numero+". Ele � Negativo.");
	}

	public static void main(String[] args) {
		
		//Declara��o das vari�veis
		double numero;
		String resultado;			
					
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner(System.in);
		System.out.println("Digite o n�mero a ser verificado:");
		numero = entrada.nextDouble();
		entrada.close();
										
		/* Chamando as fun��es que executam o processamento de dados do programa e 
		 * atribuindo seus resultados a vari�veis para exibi��o posterior ao usu�rio */
		resultado = verificar(numero);
								
		//Sa�da do processamento para o usu�rio
		System.out.println(resultado);
				
	}

}
