package selecao;

import java.util.Scanner;

public class Exercicio01 {

	//Fun��o que processa o que ser� exibido ao usu�rio
	public static String identificar(double codigo) {	
		if(codigo == 1) 
			return new String("Sul");			
		else if(codigo == 2) 
			return new String("Norte");			
		else if(codigo == 3) 
			return new String("Leste");			
		else if(codigo == 4) 
			return new String("Oeste");			
		else if(codigo == 5 || codigo == 6) 
			return new String("Nordeste");			
		else if(codigo == 7 || codigo == 8 || codigo == 9) 
			return new String("Sudeste");			
		else if(codigo == 10) 
			return new String("Centro-Oeste");
		else if(codigo == 11) 
			return new String("Noroeste");
		else 
			return new String("Importado");
	}
	
	//Bloco principal do programa
	public static void main(String[] args) {
		
		//Declara��o das vari�veis
		String regiao;
		int codigo;
		
		//Leitura dos dados a serem inseridos pelo usu�rio
		Scanner entrada = new Scanner(System.in);
		System.out.println("Insira o c�digo de origem do produto:");
		codigo = entrada.nextInt();
		entrada.close();
																				
		/* Chamando a fun��o que executa o processamento de dados do programa e 
		 * atribuindo seu resultado a uma vari�vel para exibi��o posterior ao usu�rio */
		regiao = identificar(codigo);		
			
		//Sa�da do processamento para o usu�rio
		System.out.println("Regi�o de proced�ncia do produto: "+regiao);

	}

}
